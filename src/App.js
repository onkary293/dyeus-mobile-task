import './App.css';
import { MdDone } from 'react-icons/md'
import { Navbar, Hero, Card, Banner, Button } from "./components"

function App() {
  return ( 
    <div className="App" >
      <Navbar />
      <Banner />
      <Hero title="Everyday rinse and reload" details="Achieve your dream skin goal with this complete package of skincare produts that will rejuvenate your day." />
      <div className='card__contanier'>
        <Card title="1 Months Pack (4 tubes)" savingprice={200} price={595} prevPrice={795} discount={38} cardVersion={1} />
        <div>
        </div>
        <Card title="3 Months Pack(12 tubes)" savingprice={100} price={899} prevPrice={999} discount={12} cardVersion={2} />
        <MdDone className='icon' fontSize={30} />
      </div>
      <Button />

    </div>
  );
}

export default App;