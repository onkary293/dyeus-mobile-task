import React from "react";
import { AiOutlineShoppingCart, AiOutlineMenu } from "react-icons/ai"

// Navbar created at the top of page

function Navbar() {
  return <div className="navbar">
    <AiOutlineMenu fontSize={32} />
    <AiOutlineShoppingCart fontSize={32} />
  </div>;
}

export default Navbar;
