import React from "react";
import { BsStarFill } from "react-icons/bs"

// The Two Cards of
//1 Month and 3 Month are Created in this component

function Card({ title, prevPrice, price, savingprice, discount, cardVersion }) {
  return <div className={cardVersion === 1 ? 'card1' : 'card2'}>

    {
      cardVersion === 1 &&
      (<p className="card__label">
        <BsStarFill />
        <span>MOST POPULAR</span>
        <BsStarFill />
      </p>)

    }
    <div className="card__body">
      <div>
        <h3>
          {title}
        </h3>
        <div className="card__details">
          <p className="card__savings">Savings :  ₹{savingprice}</p>
          <div className="card__values">
            <p className="card__discount">{discount}% Saved</p>
            {
              cardVersion === 1 &&
              <p className="card__offer">Best Results</p>
            }
          </div>
        </div>
      </div>
      <div className="card__price">
        <p>
          ₹{price}
        </p>
        <p>
          ₹{prevPrice}
        </p>
      </div>
    </div>
  </div>;
}

export default Card;
