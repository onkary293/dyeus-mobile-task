import React from "react";
import { BsStarFill, BsStarHalf } from "react-icons/bs"

// Hero - The Title and details are covered
//The Stars in rating section are taken from react-icons ,
//for full star we use -BsStarfill and For half - BsStarHalf

function Hero({ title, details }) {
  return <div className="hero">
    <h1 className="hero__title">{title}</h1>
    <p className="hero__details">{details}</p>
    <div className="review">
      <div className="rating">

        <BsStarFill fontSize={10} />
        <BsStarFill fontSize={10} />
        <BsStarFill fontSize={10} />
        <BsStarFill fontSize={10} />
        <BsStarHalf fontSize={10} />
      </div>
      <p>4.5</p>
      <span>Click to read reviews  &gt; </span>
    </div>
    {/* It is Price Part */}
    <div className="hero__price">
      <p>₹899</p>
      <p>₹1299</p>
    </div>
  </div>;
}

export default Hero;
