import React from "react";
import BannerImage from "../images/banner.PNG"


// Banner is the image after the navbar

function Banner() {
  return <div className="banner">
    <img src={BannerImage} alt="banner" />
  </div>;
}

export default Banner;
