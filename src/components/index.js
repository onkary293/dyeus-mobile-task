
//All of the components are exported in index.js
//It makes easy to import all the components
//once in main file i.e App.js 

export {default as Navbar} from "./Navbar"
export {default as Banner} from "./Banner"
export {default as Hero} from "./Hero"
export {default as Card} from "./Card"
export{default as Button} from "./Button"
