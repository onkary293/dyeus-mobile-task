import React from "react";
import {AiOutlineShoppingCart} from "react-icons/ai"


// The Last ADD TO CART button is Created
//The shopping Cart icon is taken from react-icons
function Button() {
  return <div className="button">
      <AiOutlineShoppingCart fontSize={25}/>
      <p>ADD TO CART</p>
  </div>;
}

export default Button;
